from config_env import return_env_config
from lib import get_project_root

cfg = return_env_config()
LOG_LEVEL = cfg.get_env_val("LOG_LEVEL", "DEBUG")
LOG_FILE_DEFAULT = str(get_project_root()) + "/config_logging/logs/pandas_sandbox.log"
LOG_FILE_JSON = str(get_project_root()) + "/config_logging/logs/pandas_sandbox_json.log"
LOG_FILE_GOAL = str(get_project_root()) + "/config_logging/logs/pandas_sandbox_goal.log"
LOG_CONFIG = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "standard": {"format": "%(asctime)s [%(levelname)s] %(name)s: %(message)s"},
        "json": {
            "format": "%(funcName)s %(message)s %(funcName)s %(asctime)s [%(levelname)s] %(lineno)i %(pathname)s ",
            "class": "pythonjsonlogger.jsonlogger.JsonFormatter",
        },
    },
    "handlers": {
        "default": {
            "level": LOG_LEVEL,
            "formatter": "standard",
            "class": "logging.StreamHandler",
        },
        "file": {
            "class": "logging.handlers.RotatingFileHandler",
            "level": LOG_LEVEL,
            "formatter": "standard",
            "filename": LOG_FILE_DEFAULT,
            "mode": "a",
            "maxBytes": 10485760,
            "backupCount": 2,
        },
        "json": {
            "class": "logging.StreamHandler",
            "formatter": "json",
        },
        "json_file": {
            "class": "logging.handlers.RotatingFileHandler",
            "level": LOG_LEVEL,
            "formatter": "json",
            "filename": LOG_FILE_JSON,
            "mode": "a",
            "maxBytes": 10485760,
            "backupCount": 2,
        },
        "goal": {
            "class": "logging.StreamHandler",
            "formatter": "json",
        },
        "goal_file": {
            "class": "logging.handlers.RotatingFileHandler",
            "level": LOG_LEVEL,
            "formatter": "json",
            "filename": LOG_FILE_GOAL,
            "mode": "a",
            "maxBytes": 10485760,
            "backupCount": 2,
        },
    },
    "loggers": {
        "reference": {
            "handlers": ["default", "file"],
            "level": LOG_LEVEL,
            "propagate": False,
        },
        "timing": {
            "handlers": ["json", "json_file"],
            "level": LOG_LEVEL,
            "propagate": False,
        },
        "goal": {
            "handlers": ["goal", "goal_file"],
            "level": LOG_LEVEL,
            "propagate": False,
        },
    },
}
