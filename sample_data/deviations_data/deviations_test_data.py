'''
STOCK =B5*(1+$O5+(RAND()-0.5)*($P5))+(B13*B21)
BOND =B6*(1+$O6+(RAND()-0.5)*($P6))+(B13*(1-B21))
'''

deviations = [{
    "mean": "0.06",
    "std_dev": "0.15",
    "uid": "deviation:asset:stock:EFGA"
}, {
    "mean": "0.06",
    "std_dev": "0.15",
    "uid": "deviation:asset:stock:ASFF"
}, {
    "mean": "0.03",
    "std_dev": "0.07",
    "uid": "deviation:asset:bond:BHKA"
}, {
    "mean": "0.00",
    "std_dev": "0.00",
    "uid": "deviation:asset:cash"
}, {
    "mean": "0.03",
    "std_dev": "0.00",
    "uid": "deviation:flow:income"
}, {
    "mean": "0.04",
    "std_dev": "0.10",
    "uid": "deviation:flow:spending"
}, {
    "mean": "0.25",
    "std_dev": "0.00",
    "uid": "deviation:flow:income-tax"
}, {
    "mean": "0.01",
    "std_dev": "0.03",
    "uid": "deviation:medical"
}, {
    "mean": "0.06",
    "std_dev": "0.00",
    "uid": "deviation:loan"
}, {
    "mean": "0.10",
    "std_dev": "0.00",
    "uid": "deviation:credit-card"
}]