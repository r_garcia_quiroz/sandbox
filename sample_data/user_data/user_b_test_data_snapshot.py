assets = [{
    "quantity": "42500.00",
    "uid": "asset:stock:EFGA",
    "allocation": ".30"
}, {
    "quantity": "7500.00",
    "uid": "asset:stock:ASFF",
    "allocation": ".30"
}, {
    "quantity": "12500.00",
    "uid": "asset:bond:BHKA",
    "allocation": ".30"
}, {
    "quantity": "7500.00",
    "uid": "asset:cash:SAFS",
    "allocation": ".10"
}]

flows = [{
    "quantity": "85000.00",
    "uid": "flow:income:3001"
},{
    "quantity": "10000.00",
    "uid": "flow:income:3002"
},{
    "quantity": "40000.00",
    "uid": "flow:spending:3002"
}]

human_q = [{
    "tolerance": "0.50",
    "uid": "human_q:risk-tolerance:4001"
}]

liabilities = [{
    "quantity": "500.00",
    "uid": "liability:medical:2001"
}, {
    "quantity": "40000.00",
    "uid": "liability:loan:2002"
}, {
    "quantity": "5000.00",
    "uid": "liability:credit-card:2003"
}]

goals = [{
    "quantity": "175000.00",
    "date": "2022-02-12",
    "uid": "goal:house:7001"
}]

snapshot = {
    "assets": assets,
    "date_of_birth": "19700923",
    "date_of_data": "20190321",
    "flows": flows,
    "goals": goals,
    "human_q": human_q,
    "liabilities": liabilities,
    "uid": "5001"
}
