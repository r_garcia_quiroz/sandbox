from sample_data.deviations_data import deviations_test_data as deviations_data
from sample_data.user_data import user_a_test_data_snapshot as user_a_data
from sample_data.user_data import user_b_test_data_snapshot as user_b_data
from sample_data.user_data import user_c_test_data_snapshot as user_c_data

DEVIATIONS_DATA_SAMPLE = deviations_data

USER_DATA_SAMPLES = {
    'user_a': user_a_data,
    'user_b': user_b_data,
    'user_c': user_c_data
}