from decimal import Decimal
from mock import patch
import unittest

from lib.utils import path_generation as path_utils

class PathGenerationTests(unittest.TestCase):

    @patch(target='lib.utils.path_generation.np.random.normal',
           return_value=Decimal('0.34'))
    def test_asset_growth(self,random_uniform):
        deviation = {
            "mean": "0.06",
            "std_dev": "0.15",
        }
        human_q = {
            "tolerance": "0.50",
        }
        fv = path_utils._asset_growth('100.00',deviation,'3000.00',human_q)
        self.assertEqual(round(fv,10), round(Decimal('1603.6'),10))


    @patch(target='lib.utils.path_generation.np.random.normal',
           return_value=Decimal('0.07'))
    def test_asset_growth_bond(self,random_uniform):
        deviation = {
            "mean": "0.03",
            "std_dev": "0.07",
        }
        human_q = {
            "tolerance": "0.50",
        }
        fv = path_utils._asset_growth_bond('100.00',deviation,'3000.00',human_q)
        self.assertEqual(round(fv,10), round(Decimal('1599.99'),10))


    @patch(target='lib.utils.path_generation.np.random.normal',
           return_value=Decimal('0.97'))
    def test_income_growth(self,random_uniform):
        deviation = {
            "mean": "0.03",
            "std_dev": "0.00",
        }
        fv = path_utils._income_growth('65000.00',deviation)
        self.assertEqual(round(fv,10), round(Decimal('66950.00'),10))


    @patch(target='lib.utils.path_generation.np.random.normal',
           return_value=Decimal('0.23'))
    def test_spending_growth(self,random_uniform):
        deviation = {
            "mean": "0.04",
            "std_dev": "0.10",
        }
        fv = path_utils._spending_growth('45000.00',deviation)
        self.assertEqual(round(fv,10), round(Decimal('45585.00'),10))


    def test_tax_growth(self):
        deviation = {
            "mean": "0.25",
            "std_dev": "0.00",
        }
        fv = path_utils._tax_growth('65000.00',deviation)
        self.assertEqual(round(fv,10), round(Decimal('16250.00'),10))