ASSET_CATEGORY = 'asset'
FLOW_CATEGORY = 'flow'

ASSET_TYPE_STOCK = 'stock'
ASSET_TYPE_BOND = 'bond'
FLOW_TYPE_INCOME = 'income'
FLOW_TYPE_INCOME_TAX = 'income-tax'
FLOW_TYPE_SPENDING = 'spending'
AGG_TYPE_NET_FLOWS = 'net-flows'
AGG_TYPE_NET_ASSETS = 'net-assets'

DATE_SERIES = 'date_series'
GROUP_SERIES_IDS = 'group_series_ids'