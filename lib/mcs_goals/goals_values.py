import numpy as np
import pandas as pd


class GoalValueAnalysis:
    def __init__(self, goals):
        if goals is not None:
            # sort goals base of time to achieve
            goals.sort(key=lambda timing: timing[1])
        else:
            raise ValueError("Goals not provided")
        self.goals = goals
        self.df = None
        self.pmt = {}
        self.generate_data_table()

    def print_goals(self):
        print(self.goals)

    def generate_data_table(self):
        """
        Generates a data table with Present value of future goals and payment
        :return:
        """
        for k, g in enumerate(self.goals):
            # if any of the goals is 0 just continue no calculation is need
            if g[0] == g[1] == 0:
                continue
            # future value of money
            pv = np.pv(g[2] / 12, g[1] * 12, 0, g[0])
            pmt = np.pmt(g[2] / 12, g[1] * 12, pv, 0)

            # fill data frame serie for payment
            if self.df is None:
                self.df = pd.DataFrame()
                self.pmt = pd.DataFrame(
                    data=np.array([[pmt, g[2]]]),
                    columns=["Goal_" + str(k), "Rate_" + str(k)],
                )
            else:
                self.pmt = self.pmt.join(
                    pd.DataFrame(
                        data=np.array([[pmt, g[2]]]),
                        columns=["Goal_" + str(k), "Rate_" + str(k)],
                    )
                )
