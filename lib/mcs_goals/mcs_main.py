import numpy as np
import pandas as pd

import locale
import matplotlib.pyplot as plots

from lib.mcs_goals.mcs_goals_value import MCS

locale.setlocale(locale.LC_ALL, "en_US.UTF-8")


def print_percentile(p_tiles_, r_tiles_):
    percentile = [x for x in range(40, 61)]
    payments = np.percentile(p_tiles_, percentile)
    rates = np.flip(np.percentile(r_tiles_, percentile))
    for p in range(len(payments)):
        print(
            " {}%-ile: ".format(percentile[p]).rjust(15),
            " {} ".format(locale.currency(payments[p], grouping=True)),
            "rate {}".format(rates[p] * 100),
        )
    print("x" * 50)


if __name__ == "__main__":

    goals_ = [(2000000, 35), (100000, 20), (75000, 10)]

    simulation = MCS(
        iterations=700,
        expected_return=0.08,
        volatility=0.18,
        time_horizon=35,
        goals=goals_,
    )

    simulation.rates.sort()
    plots.plot(simulation.rates)
    plots.show()
    plots.hist(simulation.rates, bins=50)
    plots.show()

    plots.hist(simulation.sim["Goal_0"].sort_values(), bins=1000)
    plots.axvline(
        simulation.sim["Goal_0"].mean(), color="k", linestyle="dashed", linewidth=1
    )
    plots.show()

    print(pd.DataFrame(simulation.rates).describe())

    print("x" * 50)
    print("GOAL_0")
    print(simulation.sim["Goal_0"].describe())

    print_percentile(
        p_tiles_=simulation.sim["Goal_0"], r_tiles_=simulation.sim["Rate_0"]
    )

    plots.hist(simulation.sim["Goal_1"].sort_values(), bins=1000)
    plots.axvline(
        simulation.sim["Goal_1"].mean(), color="k", linestyle="dashed", linewidth=1
    )
    plots.show()

    print("GOAL_1")
    print(simulation.sim["Goal_1"].describe())

    print_percentile(
        p_tiles_=simulation.sim["Goal_1"], r_tiles_=simulation.sim["Rate_1"]
    )

    plots.hist(simulation.sim["Goal_2"].sort_values(), bins=1000)
    plots.axvline(
        simulation.sim["Goal_2"].mean(), color="k", linestyle="dashed", linewidth=1
    )
    plots.show()

    print("GOAL_2")
    print(simulation.sim["Goal_2"].describe())

    print_percentile(
        p_tiles_=simulation.sim["Goal_2"], r_tiles_=simulation.sim["Rate_2"]
    )
