import numpy as np
import pandas as pd
from scipy import stats

from lib.mcs_goals.goals_values import GoalValueAnalysis


class MCS:
    def __init__(
        self,
        iterations=None,
        expected_return=None,
        volatility=None,
        time_horizon=None,
        goals=None,
    ):
        if not goals:
            raise ValueError("Goals not provided")
        self.goals = goals

        self.total_sim = pd.DataFrame()
        self.iterations = iterations
        self.expected_return = expected_return
        self.volatility = volatility
        self.time_horizon = time_horizon
        self.rates = []
        self.sim = pd.DataFrame()

        self._simulate()

    def _simulate(self):
        for x in range(self.iterations):
            for y in range(self.time_horizon - 1):
                rate = np.random.normal(self.expected_return, self.volatility)
                goal = GoalValueAnalysis([x + (rate,) for x in self.goals])

                self.rates.append(rate)

                try:
                    self.sim = self.sim.append(goal.pmt, ignore_index=True)
                except Exception as e:
                    print(e)
            # TODO: Add total future value and remove extra contributions after the goal time
            try:
                self.total_sim = self.total_sim.append(self.sim, ignore_index=True)
            except Exception as e:
                print(e)

        self.sim = self.sim[(np.abs(stats.zscore(self.sim)) < 1).all(axis=1)]
