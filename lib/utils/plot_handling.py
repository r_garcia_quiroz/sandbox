from datetime import datetime
import logging
from logging.config import dictConfig
import plotly as py
import plotly.graph_objs as go

import time

from config_logging import LOG_CONFIG
from lib import constants as const
from lib.utils import constants_path_generation as c_pgen
from lib.utils import constants_plot_handling as c_plot
from lib import get_project_root

logging.config.dictConfig(LOG_CONFIG)
ref_logger = logging.getLogger('reference')
json_logger = logging.getLogger('timing')

DATE_TODAY = datetime.now().date()

def get_col_number(col_header):
    '''
    Provided a column uid header in the string format type:specifier
    Ex: stock:495
    Return the column number as an integer
    :param col_header:
    :return: column number as col_number
    '''
    col_number = int(''.join(x for x in str(col_header) if x.isdigit()))
    return col_number

def get_line_weight(quantile_lookup, col):
    '''
    Provided a dataframe with quantile results and associated column numbers
    return a line weight for use in plotting
    :param quantile_lookup:
    :param col: a column header
    :return: line weight as integer
    '''
    col_int = get_col_number(col)
    col_group = str(col).split(':')[0]
    return 3 if col_int in quantile_lookup.columns and col_group == const.AGG_TYPE_NET_ASSETS else 1


def get_percentile_color(quantile_lookup, col, colors, neutral=c_plot.GRAPH_PERCENTILE_COLOR_NEUTRAL):
    '''
    Return a color to use when graphing the path associated with a given percentile.

    :param quantile_lookup: a dataframe relating selected columns in the graphing set to a given percentile
    :param col: a column header to find in percentile_df
    :param colors: a dict of color strings in rgb format with percentiles as keys
    :param neutral: a neutral color to use if none exists for the given col

    :return: a color in rgb format (str) Ex: 'rgb(177, 255, 35)'
    '''
    col_int = get_col_number(col)
    if col_int in quantile_lookup.columns:
        percentile_index_color = colors.get(quantile_lookup.at['percentile', col_int], neutral)
    else:
        percentile_index_color = neutral
    return percentile_index_color

def generate_plot(date_formatted_plot_data,quantile_lookup,goal_date,goal_amount,intersection):
    '''

    :param date_formatted_plot_data:
    :param goal_date:
    :return:
    '''

    percent_meeting_goal = (len(intersection) / c_pgen.NUMBER_OF_PERMUTATIONS) * 100

    py2_start = time.time()
    plot_data = [go.Scatter(
        x=date_formatted_plot_data[const.DATE_SERIES],
        y=date_formatted_plot_data[col],
        name=col,
        line={
            'color': get_percentile_color(quantile_lookup, col, c_plot.GRAPH_PERCENTILE_COLORS_ASSET_HIGHHLIGHTS),
            'width': get_line_weight(quantile_lookup, col),
        }
    ) for col in date_formatted_plot_data.drop(columns=[const.DATE_SERIES], axis=1).columns]

    layout_data = {
        'shapes': [{
            'type': 'line',
            'x0': goal_date,
            'y0': 0,
            'x1': goal_date,
            'y1': date_formatted_plot_data.drop(columns=[const.DATE_SERIES], axis=1).iloc[(c_pgen.DEFAULT_PERIODS - 1), 0],
            'line': {
                'color': c_plot.GRAPH_COLOR_GOAL_DATE,
                'width': 1,
                'dash': 'dashdot',
            },
        }, {
            'type': 'line',
            'x0': DATE_TODAY,
            'y0': goal_amount,
            'x1': date_formatted_plot_data.at[(c_pgen.DEFAULT_PERIODS - 1), const.DATE_SERIES],
            'y1': goal_amount,
            'line': {
                'color': c_plot.GRAPH_COLOR_GOAL_AMOUNT,
                'width': 1,
                'dash': 'dashdot',
            }
        }],
        'annotations': [
            dict(
                x=goal_date,
                y=goal_amount,
                xref='x',
                yref='y',
                text=str(len(intersection)) + ' (' + str(round(percent_meeting_goal)) + '%) of paths meeting goal',
                showarrow=True,
                font=dict(
                    family='monospace',
                    size=16,
                    color='#ffffff'
                ),
                align='center',
                arrowhead=2,
                arrowsize=1,
                arrowwidth=2,
                arrowcolor='#636363',
                ax=0,
                ay=-75,
                bordercolor='#c7c7c7',
                borderwidth=2,
                borderpad=4,
                bgcolor='rgb(0, 93, 244)',
                opacity=0.8
            )
        ]
    }

    fig = {
        'data': plot_data,
        'layout': layout_data,
    }

    py.offline.plot(fig, filename=str(get_project_root()) + '/plots/paths.html', auto_open=True)
    py2_end = time.time()
    py2_info = "%0.9f seconds to plot results" % (py2_end - py2_start)
    json_logger.info(py2_info)

    date_formatted_plot_data.to_json(str(get_project_root()) + '/plots/paths.json', orient='split')