from datetime import datetime
import logging
from logging.config import dictConfig
import time

from config_logging import LOG_CONFIG
from lib.utils import constants_path_generation as c_pgen
from pandas import MultiIndex

logging.config.dictConfig(LOG_CONFIG)
ref_logger = logging.getLogger('reference')
json_logger = logging.getLogger('timing')
goal_logger = logging.getLogger('goal')

def get_probability_to_goal(sorted_paths, goal_value, next_period_index, percentage_to_next_period):
    '''
    Returns probability score and meta data reflecting probability of meeting a goal, given a number of path permutations
    modeling potential outcomes over a given period
    :param sorted_paths:
    :param goal_value:
    :param next_period_index:
    :param percentage_to_next_period:
    :return: (probability_calculation, paths_meeting_goal_column_indexes)
    '''
    t_start = time.time()
    prev_period = sorted_paths.loc[next_period_index - 1]
    next_period = sorted_paths.loc[next_period_index]
    difference = next_period - prev_period
    difference_offset = difference * percentage_to_next_period
    target_progress_values = next_period - difference_offset
    paths_meeting_goal = target_progress_values[target_progress_values > goal_value]
    paths_meeting_goal_column_indexes = list(paths_meeting_goal.index.values)
    paths_meeting_goal_reduced_count = len(paths_meeting_goal_column_indexes)
    probability_calculation = paths_meeting_goal_reduced_count / c_pgen.NUMBER_OF_PERMUTATIONS
    goal_logger.info('paths_meeting_goal_column_indexes: ' + repr(paths_meeting_goal_column_indexes))
    goal_logger.info('probability_calculation: ' + repr(probability_calculation))
    t_end = time.time()
    t_info = "%0.9f seconds to get_probability_to_goal from %s columns of data" % (
    (t_end - t_start), len(sorted_paths.columns))
    json_logger.info(t_info)
    goal_paths = sorted_paths[paths_meeting_goal_column_indexes]
    goal_paths.columns = MultiIndex.from_product([['net-assets'], list(goal_paths.columns.values)])
    return probability_calculation, paths_meeting_goal_column_indexes, goal_paths


def delta_to_next_period(sorted_paths, date):
    '''
    Given a date series with regular intervals ( periods ) and a fixed date anywhere within the series,
    calculate the delta in days and as a percentage between the fixed date and the next period interval in the series.
    :param sorted_paths:
    :param date:
    :return: date_sort_location ( An index sort/insert location within the date series )
             percentage_of_value_to_next_period ( Delta to the next period in the date series as a decimal percentage )
    '''
    t_start = time.time()
    date_sort_location = sorted_paths.searchsorted(date)
    days_delta_to_next_period = (sorted_paths.loc[date_sort_location] - date).days
    percentage_of_value_to_next_period = days_delta_to_next_period / c_pgen.DAYS_PER_YEAR
    goal_logger.debug('date_sort_location: ' + repr(date_sort_location))
    goal_logger.debug('days_delta_to_next_period: ' + repr(days_delta_to_next_period))
    goal_logger.debug('percentage_of_value_to_next_period: ' + repr(percentage_of_value_to_next_period))
    t_end = time.time()
    t_info = "%0.9f seconds to calculate delta_to_next_period from a series of %s date intervals (periods)" % (
    (t_end - t_start), len(sorted_paths.values))
    json_logger.info(t_info)
    return date_sort_location, percentage_of_value_to_next_period

def get_goal_period_range(path_init_date,sorted_goals):
    last_goal = datetime.strptime(sorted_goals[-1]['date'], '%Y-%m-%d').date()
    return (last_goal - path_init_date).days / c_pgen.DAYS_PER_YEAR

def get_periods_between_goals(date,next_date):
    return (next_date - date).days / c_pgen.DAYS_PER_YEAR