from datetime import datetime
import functools
import logging
from logging.config import dictConfig
import pandas as pd
import time

from config_logging import LOG_CONFIG
from lib import constants as const
from lib.utils import constants_path_generation as c_pgen
from lib.utils import constants_plot_handling as c_plot
from lib.utils import path_generation as path_utils

logging.config.dictConfig(LOG_CONFIG)
ref_logger = logging.getLogger('reference')
json_logger = logging.getLogger('timing')

NEGATIVE_FLOWS = ['spending','income-tax']

def concat_column_display_names(column_names_list):
    '''
    Given a list of tuples representing multi-index dataframe columns
    return a dict for use in updating multi-index dataframe column headers

    :param column_names_list: Ex: [('stock',34),('stock',11),('stock',64)]
    :return: column_names_dict: Ex: {34: 'stock:34', 11: 'stock:11', 64: 'stock:64'}
    '''
    column_names_dict = {col_num: cat + ':' + str(col_num) for cat, col_num in column_names_list}
    return column_names_dict


def get_quantile_lookup(pivoted_paths_sorted, percentile_list):
    '''
    Given a set of sorted paths and a list of percentiles,
    return the column numbers associating a path with each percentile in the list
    :param pivoted_paths_sorted: A sorted list of paths for and asset, flow, etc...
    :param percentile_list:
    :return: Returns a quantile_lookup dataframe
    '''
    t_start = time.time()
    pivoted_paths_sorted_quantiles = pivoted_paths_sorted.loc[[c_pgen.DEFAULT_PERIODS - 1]].quantile(q=percentile_list, axis=1,
                                                                                              interpolation='nearest')
    pivoted_paths_sorted_quantiles_filtered_cols = pivoted_paths_sorted.columns[
        pivoted_paths_sorted.T[c_pgen.DEFAULT_PERIODS - 1].isin(list(pivoted_paths_sorted_quantiles[c_pgen.DEFAULT_PERIODS - 1]))]
    pivoted_paths_sorted_quantiles = pivoted_paths_sorted_quantiles.T.sort_values(by=c_pgen.DEFAULT_PERIODS - 1, axis=1,
                                                                                  ascending=False)
    quantile_lookup = pd.DataFrame([list(pivoted_paths_sorted_quantiles.columns.values)],
                                   columns=pivoted_paths_sorted_quantiles_filtered_cols, index=['percentile'])
    t_end = time.time()
    t_info = "%0.9f seconds to generate quantile_lookup from %s columns of data" % (
    (t_end - t_start), len(pivoted_paths_sorted.columns))
    ref_logger.debug('quantile_lookup: \n' + repr(quantile_lookup))
    json_logger.info(t_info)
    return quantile_lookup


def combine_frames(sorted_paths, namespace_list, combined_category_name):
    '''
    Sum all values in two dataframes with matching column headers
    :param sorted_paths: A dataframe containing paths categorized with multi-index column headings
    :param namespace_list: A list of category names to be located within the sorted_paths
    :param combined_category_name: A string name for the new combined dataframe
    :return: Returns the combined dataframe
    '''
    t_start = time.time()
    frames_to_combine = [sorted_paths[cat] if cat not in NEGATIVE_FLOWS else sorted_paths[cat] * (-1) for cat in
                         namespace_list]
    combined = functools.reduce(lambda x, y: x.combine(y, (lambda a, b: a + b), overwrite=False),
                                frames_to_combine).sort_values(by=c_pgen.DEFAULT_PERIODS - 1, axis=1, ascending=False)
    combined.columns = pd.MultiIndex.from_product([[combined_category_name], list(combined.columns.values)])
    t_end = time.time()
    t_info = "%0.9f seconds to sum %s view from %s dataframes" % (
    (t_end - t_start), combined_category_name, len(frames_to_combine))
    ref_logger.debug(combined_category_name + ': \n' + repr(combined))
    json_logger.info(t_info)
    return combined


def get_quantile_and_primary_sorts(sort_source_paths):
    '''
    Generate sorting references for paths based on a list of percentiles Ex: [0.00,0.50,1.00]
    :param sort_source_paths:
    :return:
    '''
    quantile_lookup = get_quantile_lookup(sort_source_paths, c_plot.GRAPH_PERCENTILE_LIST)
    if c_pgen.NUMBER_OF_PERMUTATIONS >= c_plot.MINIMIZE_PATH_RENDERING:
        primary_sort = list(quantile_lookup.columns.values)
    else:
        primary_sort = list(sort_source_paths.columns.values)
    ref_logger.debug('primary_sort: \n' + repr(primary_sort))
    return quantile_lookup, primary_sort


def reduce_paths(source_paths, primary_sort, category):
    '''

    :param source_paths:
    :param primary_sort:
    :param category:
    :return:
    '''
    sorted_reduced_paths = source_paths[category][primary_sort]
    sorted_reduced_paths.columns = pd.MultiIndex.from_product([[category], list(sorted_reduced_paths.columns.values)])
    return sorted_reduced_paths


def reduce_all_paths(source_paths, primary_sort):
    '''

    :param source_paths:
    :param primary_sort:
    :return:
    '''
    t_start = time.time()
    categories = list(set([col_tup[0] for col_tup in list(source_paths.columns.values)]))
    ref_logger.debug('categories: \n' + repr(categories))
    reduced_paths_for_concat = []
    for category in categories:
        reduced_paths = reduce_paths(source_paths, primary_sort, category)
        concat_dict = concat_column_display_names(reduced_paths.columns.values)
        renamed_paths = reduced_paths[category].rename(index=int, columns=concat_dict)
        reduced_paths_for_concat.append(renamed_paths)
    reduced = pd.concat(reduced_paths_for_concat, axis=1).sort_values(by=c_pgen.DEFAULT_PERIODS - 1, axis=1, ascending=False)
    t_end = time.time()
    t_info = "%0.9f seconds to reduce paths from %s columns x %s rows to %s columns x %s rows" % (
    (t_end - t_start), len(source_paths.columns.values), c_pgen.DEFAULT_PERIODS, (len(categories) * len(primary_sort)),
    c_pgen.DEFAULT_PERIODS)
    json_logger.info(t_info)
    return reduced


def get_sorted_data_set(paths, category):
    '''

    :param paths:
    :param category:
    :param return_date_series:
    :return:
    '''
    pv_category_start = time.time()
    pivoted_paths = pd.pivot_table(paths[[const.GROUP_SERIES_IDS, const.DATE_SERIES, category]], values=category, index=const.DATE_SERIES,
                                   columns=const.GROUP_SERIES_IDS).reset_index()
    pv_category_end = time.time()
    pv_category_info = "%0.9f seconds to pivot %s path data. %s rows x %s columns" % (
    (pv_category_end - pv_category_start), category, c_pgen.NUMBER_OF_PERMUTATIONS, c_pgen.DEFAULT_PERIODS)
    json_logger.info(pv_category_info)
    ref_logger.debug('pivoted_paths: \n' + repr(pivoted_paths))

    pv_category_sort_start = time.time()
    pivoted_paths_category_sorted = pivoted_paths.filter(items=[c_pgen.DEFAULT_PERIODS - 1], axis=0).drop(const.DATE_SERIES,
                                                                                                   axis=1).sort_values(
        by=c_pgen.DEFAULT_PERIODS - 1, axis=1, ascending=False)
    pv_category_sort_end = time.time()
    ref_logger.debug('pivoted_paths_category_sorted ' + category + ': \n' + repr(pivoted_paths_category_sorted))
    pv_category_sort_info = "%0.9f seconds to sort %s path data. %s rows x %s columns" % (
    (pv_category_sort_end - pv_category_sort_start), category, c_pgen.NUMBER_OF_PERMUTATIONS, c_pgen.DEFAULT_PERIODS)
    json_logger.info(pv_category_sort_info)

    pivoted_paths_unfiltered_sorted = pivoted_paths[list(pivoted_paths_category_sorted.columns.values)]
    pivoted_paths_unfiltered_sorted.columns = pd.MultiIndex.from_product(
        [[category], list(pivoted_paths_category_sorted.columns.values)])
    ref_logger.debug(str(category) + ': \n' + repr(pivoted_paths_unfiltered_sorted))

    return pivoted_paths_unfiltered_sorted


def _flow_path_permutations(data_snapshot, data_deviations, path_group_id):
    '''
    Generate a path permutations for user assets & flows
    :param data_snapshot:
    :param data_deviations:
    :param path_group_id:
    :return:
    '''
    # Flows
    #
    # Notes: Todo: build paths from variable lists of flows
    #        See: _asset_path_permutations
    path_group_series_ids = [path_group_id for i in range(c_pgen.DEFAULT_PERIODS)]
    income_flow_growth_series = path_utils.income_growth_series(data_snapshot, data_deviations)
    tax_flow_growth_series = path_utils.tax_growth_series(data_snapshot, data_deviations, income_flow_growth_series)
    spending_flow_growth_series = path_utils.spending_growth_series(data_snapshot, data_deviations)

    paths = pd.DataFrame(list(zip(path_group_series_ids,
                                  income_flow_growth_series,
                                  spending_flow_growth_series,
                                  tax_flow_growth_series)),
                         columns=[const.GROUP_SERIES_IDS,
                                  const.FLOW_TYPE_INCOME,
                                  const.FLOW_TYPE_SPENDING,
                                  const.FLOW_TYPE_INCOME_TAX])

    paths[const.FLOW_TYPE_INCOME] = pd.to_numeric(paths[const.FLOW_TYPE_INCOME])
    paths[const.FLOW_TYPE_SPENDING] = pd.to_numeric(paths[const.FLOW_TYPE_SPENDING])
    paths[const.FLOW_TYPE_INCOME_TAX] = pd.to_numeric(paths[const.FLOW_TYPE_INCOME_TAX])

    return paths


def _asset_path_permutations(asset_type_list, data_snapshot, data_deviations, net_flow_paths, path_group_id):
    '''
    Generate a path permutations for user assets & flows
    :param asset_type_list:
    :param data_snapshot:
    :param data_deviations:
    :param net_flow_paths:
    :param path_group_id:
    :return:
    '''

    # Assets
    path_group_series_ids = [path_group_id for i in range(c_pgen.DEFAULT_PERIODS)]
    assets = [path_utils.asset_growth_series(data_snapshot,
                                   data_deviations,
                                   list(net_flow_paths[path_group_id].values),
                                   asset_type) for asset_type in asset_type_list]

    column_headers = [const.GROUP_SERIES_IDS] + asset_type_list
    asset_paths_alt = pd.DataFrame(list(zip(path_group_series_ids,
                                            *assets)),
                                   columns=column_headers)

    for asset_type in asset_type_list:
        asset_paths_alt[asset_type] = pd.to_numeric(asset_paths_alt[asset_type])

    return asset_paths_alt


def _index_date_path_permutations(path_group_id,path_init_date):
    '''
    Generate a path permutations for user assets & flows
    :param data_snapshot:
    :param data_deviations:
    :param path_group_id:
    :return:
    '''
    # Indexes
    date_series = path_utils.date_series(path_init_date, c_pgen.DEFAULT_PERIODS)
    path_group_series_ids = [path_group_id for i in range(c_pgen.DEFAULT_PERIODS)]

    paths = pd.DataFrame(list(zip(path_group_series_ids,
                                  date_series)),
                         columns=[const.GROUP_SERIES_IDS,
                                  const.DATE_SERIES])

    paths[const.DATE_SERIES] = pd.to_datetime(paths[const.DATE_SERIES])

    return paths


def generate_index_date_path_data(path_init_date):
    t_start = time.time()
    index_date_paths = pd.concat([_index_date_path_permutations(i,path_init_date) for i in range(c_pgen.NUMBER_OF_PERMUTATIONS)],
                                 ignore_index=True)
    ref_logger.debug('index_date_paths: \n' + repr(index_date_paths))
    t_end = time.time()
    t_info = "%0.9f seconds to generate and load %s index and date series path permutations over %s periods" % (
    (t_end - t_start), c_pgen.NUMBER_OF_PERMUTATIONS, c_pgen.DEFAULT_PERIODS)
    json_logger.info(t_info)
    return index_date_paths


def generate_flow_path_data(data_snapshot, data_deviations):
    t_start = time.time()
    flow_paths = pd.concat(
        [_flow_path_permutations(data_snapshot, data_deviations, i) for i in range(c_pgen.NUMBER_OF_PERMUTATIONS)],
        ignore_index=True)
    ref_logger.debug('flow_paths: \n' + repr(flow_paths))
    t_end = time.time()
    t_info = "%0.9f seconds to generate and load %s flow path permutations over %s periods" % (
    (t_end - t_start), c_pgen.NUMBER_OF_PERMUTATIONS, c_pgen.DEFAULT_PERIODS)
    json_logger.info(t_info)
    return flow_paths


def generate_asset_path_data(asset_list, flow_paths, data_snapshot, data_deviations):
    t_start = time.time()
    asset_paths = pd.concat(
        [_asset_path_permutations(asset_list, data_snapshot, data_deviations, flow_paths, i) for i in
         range(c_pgen.NUMBER_OF_PERMUTATIONS)], ignore_index=True)
    ref_logger.debug('asset_paths: \n' + repr(asset_paths))
    t_end = time.time()
    t_info = "%0.9f seconds to generate and load %s asset path permutations over %s periods" % (
    (t_end - t_start), c_pgen.NUMBER_OF_PERMUTATIONS, c_pgen.DEFAULT_PERIODS)
    json_logger.info(t_info)
    return asset_paths


def get_combined_flows(flow_path_data, flow_list, index_date_path_data):
    '''
    Given a list of flow uids generate and return all flow data, including combined/net
    :param flow_list:
    :return:
    '''
    indexed_flow_paths = pd.concat([index_date_path_data, flow_path_data], axis=1)
    flow_data_sorted = [get_sorted_data_set(indexed_flow_paths, flow) for flow in flow_list]
    sorted_data_concatentated = pd.concat(flow_data_sorted, axis=1)
    combined_flows = combine_frames(sorted_data_concatentated, flow_list, const.AGG_TYPE_NET_FLOWS)
    all_flow_categories = pd.concat([sorted_data_concatentated, combined_flows], axis=1)

    return all_flow_categories


def get_combined_assets(asset_path_data, asset_list, index_date_path_data):
    '''

    :param asset_list:
    :param combined_flows:
    :return:
    '''
    indexed_asset_paths = pd.concat([index_date_path_data, asset_path_data], axis=1)
    asset_data_sorted = [get_sorted_data_set(indexed_asset_paths, asset) for asset in asset_list]
    sorted_data_concatentated = pd.concat(asset_data_sorted, axis=1)
    combined_assets = combine_frames(sorted_data_concatentated, asset_list, const.AGG_TYPE_NET_ASSETS)
    all_asset_categories = pd.concat([sorted_data_concatentated, combined_assets], axis=1)

    return all_asset_categories