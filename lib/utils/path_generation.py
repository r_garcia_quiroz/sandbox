from datetime import timedelta
from decimal import Decimal
import logging
from logging.config import dictConfig
import numpy as np
import time

from config_logging import LOG_CONFIG
from lib.utils import constants_path_generation as c_pgen

logging.config.dictConfig(LOG_CONFIG)
ref_logger = logging.getLogger('reference')
json_logger = logging.getLogger('timing')

def add_days_to_date(today, number_of_days):
    '''
    Given a date and a number of days to add to it,
    calculate and return the future date.

    :param today: a python datetime.date formatted
    :param number_of_days: number of days to add integer formatted

    :return: python datetime.date
    '''
    return today + timedelta(days=number_of_days)

def get_by_uid(data, uid):
    '''
    Get an item from a dict by its uid namespace using the structure
    category:type:specifier

    :param data: a list of dicts with a unique id namespace ( uid ):
    :param uid: a uid ( or partial ) to be matched for return
    :return: first item in the dict matching the provided uid namespace
    '''
    return next((item for item in data if item['uid'][:len(uid)] == uid), None)

def get_by_uid_v2(data, uid):
    '''
    Get an item from a dict by its uid namespace using the structure
    category:type:specifier
    If the specified uid is not found, return the first item matching its type

    :param data: a list of dicts with a unique id namespace ( uid ):
    :param uid: a uid ( or partial ) to be matched for return
    :return: first item in the dict matching the provided uid namespace
    '''
    # Exact match
    u_item = next((item for item in data if item['uid'] == uid), None)
    if u_item is None:
        # Partial uid match to first available more specific of type
        u_item = next((item for item in data if item['uid'][:len(uid)] == uid), None)
        # if u_item is None:
        #     # Overly specified uid match to first available of type

    return u_item

def _asset_growth(quantity, deviations, net_cash_flow, human_q, allocation):
    '''

    :param quantity:
    :param deviations:
    :param net_cash_flow:
    :param human_q:
    :return:
    '''
    return (Decimal(quantity) * (Decimal(1.00) + Decimal(deviations['mean']) +
           ((Decimal(np.random.normal(0, 1)) - Decimal(0.50)) * Decimal(deviations['std_dev']))) +
           (Decimal(net_cash_flow) * Decimal(human_q['tolerance']) * Decimal(allocation)))

def _asset_growth_bond(quantity, deviations, net_cash_flow, human_q, allocation):
    '''

    :param quantity:
    :param deviations:
    :param net_cash_flow:
    :param human_q:
    :return:
    '''
    return (Decimal(quantity) * (Decimal(1.00) + Decimal(deviations['mean']) +
           ((Decimal(np.random.normal(0, 1)) - Decimal(0.50)) * Decimal(deviations['std_dev']))) +
           (Decimal(net_cash_flow) * Decimal(human_q['tolerance']) * Decimal(allocation)))

def asset_growth_series(snapshot, deviations, net_flow_series, uid, periods=c_pgen.DEFAULT_PERIODS):
    '''

    :param snapshot:
    :param deviations:
    :param net_flow_series:
    :param uid:
    :param periods:
    :return:
    '''
    t_start = time.time()
    asset = get_by_uid_v2(snapshot['assets'], 'asset:' + uid)
    asset_deviations = get_by_uid(deviations, 'deviation:asset:' + uid)
    human_q = get_by_uid(snapshot['human_q'], 'human_q:risk-tolerance')
    series = [Decimal(asset['quantity'])]
    allocation = Decimal(asset['allocation'])
    for i in range(periods):
        # Notes: Todo: this if condition is temporary and should be replaced with a dispatch/method lookup in the near future.
        if 'stock' in uid:
            series.append(_asset_growth(series[i], asset_deviations, net_flow_series[i], human_q, allocation))
        else:
            series.append(_asset_growth_bond(series[i], asset_deviations, net_flow_series[i], human_q, allocation))
    t_end = time.time()
    t_info = "%0.9f seconds to generate %s results. \n" % ((t_end - t_start), periods)
    # json_logger.debug('_asset_growth_series t_info: ' + t_info)
    return series


def date_series(today, periods):
    '''

    :param today:
    :param periods:
    :return:
    '''
    date_series = [today]
    for i in range(periods):
        date_series.append(add_days_to_date(date_series[i], c_pgen.DAYS_PER_YEAR))
    return date_series


def _income_growth(income, income_deviations):
    '''

    :param income:
    :param income_deviations:
    :return:
    '''
    # Note as it stands here, all income paths will be identical
    # We assume that this formula will change over time and the paths will diverge.
    return (Decimal(income) * (Decimal(1.00) + Decimal(income_deviations['mean']) +
           ((Decimal(np.random.normal(0, 1)) - Decimal(0.50)) * Decimal(income_deviations['std_dev']))))


def income_growth_series(snapshot, deviations, periods=c_pgen.DEFAULT_PERIODS):
    '''

    :param snapshot:
    :param deviations:
    :param periods:
    :return:
    '''
    t_start = time.time()
    income_flow = get_by_uid(snapshot['flows'], 'flow:income')
    income_flow_deviations = get_by_uid(deviations, 'deviation:flow:income')
    series = [Decimal(income_flow['quantity'])]
    for i in range(periods):
        series.append(_income_growth(series[i], income_flow_deviations))
    t_end = time.time()
    t_info = "%0.9f seconds to generate %s results. \n" % ((t_end - t_start), periods)
    # json_logger.debug('_income_growth_series t_info: ' + t_info)
    return series


def _spending_growth(spending, spending_deviations):
    '''

    :param spending:
    :param spending_deviations:
    :return:
    '''
    return (Decimal(spending) * (Decimal(1.00) + Decimal(spending_deviations['mean']) +
           ((Decimal(np.random.normal(0, 1)) - Decimal(0.50)) * Decimal(spending_deviations['std_dev']))))


def spending_growth_series(snapshot, deviations, periods=c_pgen.DEFAULT_PERIODS):
    '''

    :param snapshot:
    :param deviations:
    :param periods:
    :return:
    '''
    t_start = time.time()
    spending_flow = get_by_uid(snapshot['flows'], 'flow:spending')
    spending_flow_deviations = get_by_uid(deviations, 'deviation:flow:spending')
    series = [Decimal(spending_flow['quantity'])]
    for i in range(periods):
        series.append(_spending_growth(series[i], spending_flow_deviations))
    t_end = time.time()
    t_info = "%0.9f seconds to generate %s results. \n" % ((t_end - t_start), periods)
    # json_logger.debug('_spending_growth_series t_info: ' + t_info)
    return series


def _tax_growth(income, tax_flow_deviations):
    '''

    :param income:
    :param tax_flow_deviations:
    :return:
    '''
    return Decimal(income) * Decimal(tax_flow_deviations['mean'])


def tax_growth_series(snapshot, deviations, income_flow_series, periods=c_pgen.DEFAULT_PERIODS):
    '''

    :param snapshot:
    :param deviations:
    :param periods:
    :return:
    '''
    t_start = time.time()
    income = get_by_uid(snapshot['flows'], 'flow:income')
    tax_flow_deviations = get_by_uid(deviations, 'deviation:flow:income-tax')
    series = [Decimal(income['quantity']) * Decimal(tax_flow_deviations['mean'])]
    for i in range(periods):
        series.append(_tax_growth(income_flow_series[i], tax_flow_deviations))
    t_end = time.time()
    t_info = "%0.9f seconds to generate %s results. \n" % ((t_end - t_start), periods)
    # json_logger.debug('_tax_growth_series t_info: ' + t_info)
    return series