import sys, os

BASE_PATH = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(BASE_PATH, '../'))

import argparse
from datetime import datetime
from decimal import Decimal
import logging
from logging.config import dictConfig
import pandas as pd

from config_logging import LOG_CONFIG
from lib import constants as const
from lib.utils import constants_path_generation as c_pgen
from lib.utils import frame_handling as frame_utils
from lib.utils import goal_analysis as goal_utils
from lib.utils import plot_handling as plot_utils
from sample_data import data_utils

logging.config.dictConfig(LOG_CONFIG)
ref_logger = logging.getLogger('reference')
json_logger = logging.getLogger('timing')
goal_logger = logging.getLogger('goal')

parser = argparse.ArgumentParser(description='SANDBOX')
parser.add_argument('-snp', "--snap", required=False, default='user_a',
                    help="A test user snapshot from /tests/test_data")


def main(path_init_date):
    args = parser.parse_args()
    USER_SAMPLE = data_utils.USER_DATA_SAMPLES.get(args.snap)

    if USER_SAMPLE is not None:
        index_date_path_data = frame_utils.generate_index_date_path_data(path_init_date)
        index = index_date_path_data.drop([const.GROUP_SERIES_IDS], axis=1).head(c_pgen.DEFAULT_PERIODS)

        flow_path_data = frame_utils.generate_flow_path_data(USER_SAMPLE.snapshot,
                                                             data_utils.DEVIATIONS_DATA_SAMPLE.deviations).drop(
            [const.GROUP_SERIES_IDS], axis=1)
        flows = frame_utils.get_combined_flows(flow_path_data, [const.FLOW_TYPE_INCOME, const.FLOW_TYPE_SPENDING,
                                                                const.FLOW_TYPE_INCOME_TAX],
                                               index_date_path_data)

        asset_path_uid_list = [asset['uid'][len('asset:'):] for asset in USER_SAMPLE.snapshot['assets'] if
                               'cash' not in asset['uid']]
        asset_path_data = frame_utils.generate_asset_path_data(asset_path_uid_list,
                                                               flows[const.AGG_TYPE_NET_FLOWS],
                                                               USER_SAMPLE.snapshot,
                                                               data_utils.DEVIATIONS_DATA_SAMPLE.deviations).drop(
            [const.GROUP_SERIES_IDS],
            axis=1)
        assets = frame_utils.get_combined_assets(asset_path_data, asset_path_uid_list, index_date_path_data)

        all_paths_concat = pd.concat([flows, assets], axis=1)
        quantile_lookup, primary_sort = frame_utils.get_quantile_and_primary_sorts(assets[const.AGG_TYPE_NET_ASSETS])
        paths_to_plot = all_paths_concat.drop(
            columns=[const.FLOW_TYPE_INCOME, const.FLOW_TYPE_SPENDING, const.FLOW_TYPE_INCOME_TAX], axis=1)
        reduced_plot_data = frame_utils.reduce_all_paths(paths_to_plot, primary_sort)
        date_formatted_plot_data = pd.concat([index, reduced_plot_data], axis=1)

        goal_date = datetime.strptime(USER_SAMPLE.snapshot['goals'][0]['date'], '%Y-%m-%d')
        goal_amount = Decimal(USER_SAMPLE.snapshot['goals'][0]['quantity'])
        next_period_index, percentage_to_next_period = goal_utils.delta_to_next_period(index[const.DATE_SERIES],
                                                                                       goal_date)
        probability_score, goal_path_sort, goal_paths = goal_utils.get_probability_to_goal(
            assets[const.AGG_TYPE_NET_ASSETS],
            goal_amount,
            next_period_index, percentage_to_next_period)


        quantile_lookup_goal, primary_sort_goal = frame_utils.get_quantile_and_primary_sorts(goal_paths[const.AGG_TYPE_NET_ASSETS])
        reduced_plot_data_goal = frame_utils.reduce_all_paths(goal_paths, primary_sort_goal)
        date_formatted_plot_data_goal = pd.concat([index, reduced_plot_data_goal], axis=1)

        intersection = list(set(list(assets[const.AGG_TYPE_NET_ASSETS].columns.values)) & set(goal_path_sort))


        # plot_utils.generate_plot(date_formatted_plot_data, quantile_lookup, goal_date, goal_amount, intersection)

        plot_utils.generate_plot(date_formatted_plot_data_goal, quantile_lookup_goal, goal_date, goal_amount, intersection)

        ref_logger.debug('index_date_path_data: \n' + repr(index_date_path_data))
        ref_logger.debug('index: \n' + repr(index))
        ref_logger.debug('asset_path_uid_list: ' + repr(asset_path_uid_list))
        goal_logger.info('number of paths meeting goal: ' + repr(len(intersection)))

        date_sorted_goals = sorted(USER_SAMPLE.snapshot['goals'], key=lambda goal:goal['date'], reverse=False)
        ref_logger.debug('date_sorted_goals: \n' + repr(date_sorted_goals))

        goal_period_range = goal_utils.get_goal_period_range(path_init_date,date_sorted_goals)
        ref_logger.debug('goal_period_range: \n' + repr(goal_period_range))

if __name__ == '__main__':
    '''
    Initiate with current date, but any date can be used.
    '''
    main(datetime.now().date())
