import os
import yaml

class EnvConfig(object):
	"""
	Normalizes yaml and os.env configuration

	"""

	def __init__(self, config_file_path=None):
		"""
		:param config_file_path: Path to file Type: yaml config or None

		"""
		self.config_file_path = config_file_path
		self.yaml_config = None

		if self.config_file_path is not None:
			self.yaml_config = None

	def _read_yaml(self, config_file_path):
		"""
		:param config_file_path: Path to file Type: yaml config

		"""
		try:
			with open(config_file_path, 'r') as ymlfile:
				return yaml.load(ymlfile)
		except Exception as e:
			return None

	def _get_yaml_env(self, key):
		"""
		Get and environment variable from yaml config
		:param key: String
		"""
		try:
			return self.yaml_config(key)
		except Exception as e:
			return None

	def get_env_val(self, key, default=''):
		"""
		Get environment variable
		Trying first from yaml_config else os.getenv else default
		:param key: String
		"""
		val = self._get_yaml_env(key)
		if val is None:
			val = os.getenv(key, default)

		return val

def return_env_config():
	"""
	Configure app based on APP_ENV_CONTEXT or default
	Expects APP_ENV_CONTEXT to be possible val: 'dev', 'qa', 'stage' or 'prod'
	"""
	app_env_context = os.getenv('APP_ENV_CONTEXT', None)

	try:
		yaml_config_path = os.path.join(os.path.dirname(__file__),
										'../../environment',
										'%s.yml' % app_env_context.lower())
	except Exception as e:
		yaml_config_path = None

	return EnvConfig(config_file_path=yaml_config_path)

