from config_env import return_env_config

cfg = return_env_config()

AWS_S3_REGION = cfg.get_env_val('AWS_S3_REGION', 'us-east-1')
